package com.ljg.userserver.services.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ljg.userserver.models.AccountSearchModel;
import com.ljg.common.utils.response.ResponsePageResult;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;
import com.ljg.userserver.services.AccountTblService;
import com.ljg.userserver.mappers.AccountTblMapper;
import com.ljg.userserver.pojo.AccountTblDTO;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;

@Log4j2
@Service
public class AccountTblServiceImpl extends ServiceImpl<AccountTblMapper,AccountTblDTO> implements AccountTblService{

    @Override
    @Transactional
    public boolean debit(Long userId, BigDecimal money) {
        log.info("开始扣款");
        try {
            int debit = this.baseMapper.debit(userId, money);
            log.info("扣款成功");
            return debit == 1;
        } catch (Exception e) {
            throw new RuntimeException("扣款失败，可能是余额不足！");
        }
    }

    /**
     * 添加用户
     *
     * @param accountTbl
     */
    @Override
    public boolean add(AccountTblDTO accountTbl) {
        return this.baseMapper.insert(accountTbl) == 1 ;
    }


    /**
     * 查询用户
     *
     * @param model
     * @return
     */
    @Override
    public ResponsePageResult<AccountTblDTO> search(AccountSearchModel model) {
        return model.queryPage(this);
    }

}