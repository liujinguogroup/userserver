package com.ljg.userserver.models;

import io.swagger.annotations.ApiModel;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

@Data
@ApiModel(value = "ReduceMoneyModel", description = "实体")
public class ReduceMoneyModel {

    private Long userId;
    private BigDecimal money;
}

