package com.ljg.userserver.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.Builder;
import lombok.experimental.Tolerate;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.math.BigDecimal;


@Data
@Builder
@ApiModel(value = "AccountTblDTO",description = "")
@TableName(value = "account_tbl", autoResultMap = true)
public class AccountTblDTO {

	@Tolerate
	public AccountTblDTO(){
	}
	 /**
	 * 主键
	 * */
	@ApiModelProperty("主键")
	@TableId(value = "id", type = IdType.AUTO)
	private Long id;

	 /**
	 * 手机号码
	 * */
	@ApiModelProperty("手机号码")
	@TableField("phone")
	private String phone;

	 /**
	 * 密码，加密存储
	 * */
	@ApiModelProperty("密码，加密存储")
	@TableField("password")
	private String password;

	 /**
	 * 昵称，默认是用户id
	 * */
	@ApiModelProperty("昵称，默认是用户id")
	@TableField("nick_name")
	private String nickName;

	 /**
	 * 人物头像
	 * */
	@ApiModelProperty("人物头像")
	@TableField("icon")
	private String icon;

	 /**
	 * 创建时间
	 * */
	@ApiModelProperty("创建时间")
	@TableField("create_time")
	private Long createTime;

	 /**
	 * 更新时间
	 * */
	@ApiModelProperty("更新时间")
	@TableField("update_time")
	private Long updateTime;

	 /**
	 * 账户余额
	 * */
	@ApiModelProperty("账户余额")
	@TableField("money")
	private BigDecimal money;

}