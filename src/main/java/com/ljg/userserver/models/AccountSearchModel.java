package com.ljg.userserver.models;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.ljg.userserver.pojo.AccountTblDTO;
import com.ljg.common.mybatisplus.IPageSearchModel;
import lombok.Data;

import java.io.Serializable;

@Data
public class AccountSearchModel extends IPageSearchModel<AccountTblDTO> implements Serializable {

    private String userId;

    /**
     * 装载查询条件
     *
     * @return
     */
    @Override
    public QueryWrapper<AccountTblDTO> toQueryWrapper() {
        QueryWrapper<AccountTblDTO> wrapper = new QueryWrapper<>();
        wrapper.lambda().eq(AccountTblDTO::getId,userId);
        return wrapper;
    }
}

