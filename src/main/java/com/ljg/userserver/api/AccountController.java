package com.ljg.userserver.api;

import com.ljg.userserver.models.AccountSearchModel;
import com.ljg.userserver.models.ReduceMoneyModel;
import com.ljg.userserver.pojo.AccountTblDTO;
import com.ljg.userserver.services.AccountTblService;
import com.ljg.common.utils.response.ResponsePageResult;
import com.ljg.common.utils.response.ResponseResult;


import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@Api
@RestController
public class AccountController {

    @Autowired
    private AccountTblService accountTblService;

    @ApiOperation(value = "用户减余额 ")
    @ApiParam(value = "查询条件", name = "model", type = "com.example.ljg.models.ReduceMoneyModel")
    @PostMapping("/account/reduceMoney")
    public ResponseResult<String> reduceMoney(@RequestBody ReduceMoneyModel model){
        return ResponseResult.success(accountTblService.debit(model.getUserId(), model.getMoney()));
    }

    @PostMapping("/account/add")
    public ResponseResult<Boolean> add(@RequestBody AccountTblDTO accountTbl){
        return ResponseResult.success(accountTblService.add(accountTbl));
    }

    @PostMapping("/account/search")
    public ResponsePageResult<AccountTblDTO> search(@RequestBody AccountSearchModel model){
        return accountTblService.search(model);
    }

}
