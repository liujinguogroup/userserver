package com.ljg.userserver.mappers;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ljg.userserver.pojo.AccountTblDTO;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Update;

import java.math.BigDecimal;

public interface AccountTblMapper extends BaseMapper<AccountTblDTO>{
    @Update("update account_tbl set money = money - ${money} where id = #{userId}")
    int debit(@Param("userId") Long userId, @Param("money") BigDecimal money);
}