package com.ljg.userserver.services;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ljg.userserver.models.AccountSearchModel;
import com.ljg.userserver.pojo.AccountTblDTO;
import com.ljg.common.utils.response.ResponsePageResult;

import java.math.BigDecimal;

public interface AccountTblService extends IService<AccountTblDTO>{
    /**
     * 从用户账户中借出
     */
    boolean debit(Long userId, BigDecimal money);

    /**
     * 用户添加
     */
    boolean add(AccountTblDTO accountTbl);

    ResponsePageResult<AccountTblDTO> search(AccountSearchModel model);
}