package com.ljg.userserver;
import org.apache.commons.io.FileUtils;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mock.web.MockHttpServletResponse;;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import java.io.File;
import java.nio.file.Paths;
import java.nio.charset.Charset;
import java.net.URI;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.MOCK)
class CreateApiDocTests {
	@Autowired
	private WebApplicationContext wac;

	private MockMvc mvc;

	@Test
	void contextLoads() throws Exception {
		mvc = MockMvcBuilders.webAppContextSetup(wac).build();
		ResultActions ra = mvc.perform(MockMvcRequestBuilders.get(new URI("/v2/api-docs")));
		MvcResult result = ra.andReturn();
		MockHttpServletResponse response=result.getResponse();
		if(response.getStatus()==200){
			String content=response.getContentAsString(Charset.forName("UTF-8"));
			String rootDir = System.getProperty("user.dir");
			            File apiFile = Paths.get(rootDir, "api.json").toFile();
			FileUtils.write(apiFile, content, "UTF-8");
		}else{
			throw  new Exception("request /v2/api-docs error.");
		}
	}
}
